//
//  main.m
//  GitlabCIProject
//
//  Created by OPS on 11/7/17.
//  Copyright © 2017 regie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
