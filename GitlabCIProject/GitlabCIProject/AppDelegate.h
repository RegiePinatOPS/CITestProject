//
//  AppDelegate.h
//  GitlabCIProject
//
//  Created by OPS on 11/7/17.
//  Copyright © 2017 regie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

